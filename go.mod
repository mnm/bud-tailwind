module gitlab.com/mnm/bud-tailwind

go 1.17

require gitlab.com/mnm/bud v0.0.0-20211017185247-da18ff96a31f

require (
	github.com/evanw/esbuild v0.12.24 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211117180635-dee7805ff2e1 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
