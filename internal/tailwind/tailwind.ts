import postcss from "postcss"
import tailwindcss from "tailwindcss/lib/processTailwindFeatures"
import resolveConfig from "tailwindcss/resolveConfig"
import expandApplyAtRules from "tailwindcss/lib/jit/lib/expandApplyAtRules"

// https://github.com/tailwindlabs/play.tailwindcss.com/blob/master/src/workers/processCss.js

const transform = postcss([
  tailwindcss(
    resolveConfig({
      mode: "jit",
    })
  ),
])

export default async function process(css) {
  return transform.process(css).css
}
