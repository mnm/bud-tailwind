package tailwind

import (
	"bytes"
	"fmt"
	"os/exec"

	"gitlab.com/mnm/bud/go/mod"
	"gitlab.com/mnm/bud/transform"
)

func New(module *mod.Module) *Transform {
	return &Transform{module}
}

type Transform struct {
	module *mod.Module
}

func (t *Transform) SvelteToSvelte(file *transform.File) error {
	cmd := exec.Command("./node_modules/.bin/tailwindcss", "--jit", "--purge", file.Path())
	cmd.Dir = t.module.Directory()
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("%s\ntailwind: %w", stderr, err)
	}
	// TODO: make this more robust. Unfortunately, we'll probably need a custom
	// html renderer because golang.org/x/net/html.Render breaks Svelte code.
	start := bytes.LastIndex(file.Code, []byte("<style>"))
	end := bytes.LastIndex(file.Code, []byte("</style>"))
	if start < 0 || end < 0 {
		code := bytes.NewBuffer(file.Code)
		code.WriteString("\n<style>")
		code.Write(stdout.Bytes())
		code.WriteString("\n</style>")
		file.Code = code.Bytes()
		return nil
	}
	start += 7 // Include <style>
	code := new(bytes.Buffer)
	code.Write(file.Code[0:start])
	code.WriteString("\n")
	code.Write(stdout.Bytes())
	code.WriteString("\n")
	code.Write(file.Code[start:])
	file.Code = code.Bytes()
	return nil
}
